<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        .infor {
            padding-top: 1em;
        }
        .mb-0 {

        }
    </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <h2>Mail from WeInvestInAfrica</h2>

    <p class="lead text-secondary">
        We're sorry, your project on <b>{{ $project_name }}</b> has been rejected.
    </p>
    <p class="lead text-secondary">
        May be try next time...
    </p>

</body>
</html>
