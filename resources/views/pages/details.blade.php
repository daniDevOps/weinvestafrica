@extends('layouts.app')

@section('content')

    <div id="detailspage">
       {{-- Header section --}}
       <div id="detailshead">
            <div class="container">
                <div class="row mt-5">
                    <div class="col-md-12">
                        <div class="headcenter">
                            <div class="row align-items-center">
                                <div class="col-md-10">
                                    <h1 class="">{{ $project->name }}</h1>
                                    <p class="lead text-secondary">{{ date('M d, Y', $project->created_at->timestamp) }}</p>
                                </div>
                                <div class="col-md-2">
                                    <a  href="#investmentprjt" class="btn investbtn btn-lg" data-target="#collapseform" data-toggle="collapse" aria-expanded="true" aria-controls="collapseform" >Invest</a>
                                </div>
                            </div>
                        </div>
                        <hr class="my-4">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                             <div class="d-flex align-items-center justify-content-between">
                                 <h5 class="font-weight-bold">{{ \App\Category::find($project->category_id)->name }}</h5>
                                 <h3>$ {{ number_format($project->investment + (($project->roi/100)*$project->investment)) }}</h3>
                             </div>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        </div>

        {{-- Body --}}
        <div id="details-content">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Description</h4>

                        <div class="text-wrap d-inline-block my-3 text-justify" style="width: 100%">
                            {!! $project->description !!}
                        </div>


                        <img src="{{ $project->getImageURL()  }}" class="py-3 img-fluid" style="width: 100%" alt="">

                        <div class="mt-4">
                            <h4 class="font-weight-bold">Category</h4>
                            <h5 class="">{{ \App\Category::find($project->category_id)->name }}</h5>
                        </div>

                        <div  id="investmentprjt" class="mt-5">
                            <h4 class="font-weight-bold">Investment</h4>
                            <h5 class="">$ {{ number_format($project->investment) }}</h5>
                        </div>

                        {{-- The form --}}
                        <div class="accordionform mt-5 mb-5" id="accordionform">
                            <div class="card">
                                <div class="" id="headingOne">
                                    <h2 class="mb-0 text-center">
                                        <button class="btn btn-lg btn-block" type="button" data-toggle="collapse" data-target="#collapseform" aria-expanded="true" aria-controls="collapseform">
                                            Invest
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseform" class="collapse " aria-labelledby="headingOne" data-parent="#accordionform">
                                    <div class="card-body">
                                        <h3>Investment Review</h3>
                                        <form action="{{ route('invest', $project->id) }}" method="POST" role="email">
                                            @csrf

                                            <div class="form-group">
                                                <input type="text" name="investorname" value="{{ old('investorname') }}" placeholder="Your Name" class="form-control @error('investorname') is-invalid @enderror">

                                                @error('investorname')
                                                    <span class="invalid-feedback">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input type="email" name="investoremail" value="{{ old("investoremail") }}" placeholder="Your E-mail" class="form-control @error('investoremail') is-invalid @enderror">

                                                @error('investoremail')
                                                    <span class="invalid-feedback">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input type="phone" name="investorphone" value="{{ old("investorphone") }}" placeholder="Your Phone Number" class="form-control @error('investorphone') is-invalid @enderror">

                                                @error('investorphone')
                                                    <span class="invalid-feedback">{{ $message }}</span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <textarea name="investordesc" id="investordesc" cols="30" rows="10" value="{{ old('investordesc') }}" class="form-control" placeholder="Your Comment"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-block sendinvestbtn">Send</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">

                            <div class="col-md-12 px-0">
                                <h2 class="card-header">Project Info</h2>

                                <div class="card-body">

                                    <div class="">
                                        <span class="d-flex ">
                                            <label for="email">Duration: </label>
                                            <p class="text-secondary pl-2">{{ $project->duration }} months</p>
                                        </span>
                                    </div>

                                    <div class="">
                                        <span class="d-flex ">
                                            <label for="email">ROI: </label>
                                            <p class="text-secondary pl-2">{{ $project->roi }}x</p>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card mt-5">

                            <div class="col-md-12 px-0">
                                <h2 class="card-header">Location</h2>

                                <div class="card-body">
                                    <div class="mt-3">
                                        <h3>{{ $project->country }}</h3>
                                        <p class="lead text-secondary">{{ $project->address }}</p>
                                    </div>

                                    <div class="">
                                        <span class="d-flex ">
                                            <label for="email">E-mail: </label>
                                            <p class="text-secondary pl-2">{{ $project->email }}</p>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
