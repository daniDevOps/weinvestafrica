@extends('admin.layouts.app')
@section('title', $post->name)

@section('header-style')
    <style>
        .summernote {
            background: transparent;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
        .summernote .btn {
            color: rgba(0, 0, 0, .54)!important;
            background-color: transparent!important;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
        .summernote {
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.blogs.index') }}">Blog</a></li>
                        <li class="breadcrumb-item active">{{ $post->title }}</li>
                    </ol>
                </div>
                <h4 class="page-title">Edit: {{ $post->title }}</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Edit Post</h5>
            <form method="POST" action="{{ route('admin.blogs.update', $post->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')

                <div class="form-group">
                    <label>Title <em>*</em></label>
                    <input type="text" name="title"
                           value="{{ old('title', $post->title) }}"
                           class="form-control @error('title') is-invalid @enderror">
                    @error('title')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Image</label>

                    <div class="form-control">
                        <input id="image" type="file" name="image" class="hidden @error('image') is-invalid @enderror" style="width: 6.5em;">
                        <label id="filename" for="image">{{ request()->image ? basename(request()->image) : basename($post->getImageURL()) }}</label>
                    </div>

                    @error('image')
                        <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group summernote">
                    <label>Description</label>
                    <textarea id="blog-summernote" class="form-control @error('description') is-invalid @enderror"
                              name="description" rows="5">{{ old('description', $post->description) }}</textarea>
                    @error('description')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>

@endsection

@section('footer_script')
    <script>
        $('#blog-summernote').summernote({
            minHeight: 200,
            placeholder: 'Write here ...',
            focus: false,
            airMode: false,
            fontNames: ['Roboto', 'Calibri', 'Times New Roman', 'Arial'],
            fontNamesIgnoreCheck: ['Roboto', 'Calibri'],
            dialogsInBody: true,
            dialogsFade: true,
            disableDragAndDrop: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['style', 'ul', 'ol', 'paragraph']],
                ['fontsize', ['fontsize']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['height', ['height']],
                ['misc', ['undo', 'redo', 'print', 'help', 'fullscreen']]
            ],
            popover: {
                air: [
                ['color', ['color']],
                ['font', ['bold', 'underline', 'clear']]
                ]
            },
            print: {
                //'stylesheetUrl': 'url_of_stylesheet_for_printing'
            }
        });
        $("#image").on('change', () => {
            var image = $("input#image").val()
            $("label#filename").text(basename(image, "\\"))
        })

        function basename(str, sep) {
            return str.substr(str.lastIndexOf(sep) + 1);
        }
    </script>
@endsection
