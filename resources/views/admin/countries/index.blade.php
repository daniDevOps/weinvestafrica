@extends('admin.layouts.app')
@section('title', 'Countries')

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">Countries</li>
                    </ol>
                </div>
                <h4 class="page-title">Countries</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Countries</h5>
                    <div class="table-responsive">
                        <table class="table table-stripped">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Country</td>
                                <td>Projects</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($countries as $country)
                                @if(!$country->voided)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $country->name }}</td>
                                        <td>{{ App\Project::where('country_id', $country->id)->count() }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-info btn-sm" title="view more"
                                                        data-toggle="modal" data-target="#view{{ $country->id }}">
                                                    <i class="fa fa-eye"></i></button>
                                                <a href="{{ route('admin.countries.edit', ['country' => $country->id]) }}"
                                                   class="btn btn-light btn-sm" title="Edit"><i
                                                            class="fa fa-pen"></i></a>
                                                <button class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                                                        data-target="#delete{{ $country->id }}">
                                                    <i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>

                                    <div id="delete{{ $country->id }}" class="modal fade" tabindex="-1" role="dialog"
                                         aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content modal-filled bg-danger">
                                                <div class="modal-body p-4">
                                                    <form method="POST"
                                                          action="{{ route('admin.countries.destroy', ['country' => $country->id]) }}">
                                                        @method('DELETE')
                                                        @csrf
                                                        <div class="text-center">
                                                            <i class="dripicons-wrong h1 text-white"></i>
                                                            <h4 class="mt-2 text-white">Delete {{ $country->name }}
                                                                !</h4>
                                                            <p class="mt-3 text-white">
                                                                Are you sure you want to delete this country?
                                                                All projects under this country will no longer be
                                                                available
                                                                but this action is reversible</p>
                                                            <button type="submit" class="btn btn-light my-2">Proceed
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                @else
                                    <tr class="text-muted">
                                        <td class="strike-through">{{ $loop->index + 1 }}</td>
                                        <td class="strike-through">{{ $country->name }}</td>
                                        <td class="strike-through">{{ $country->projects->count() }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-info btn-sm" title="view more"
                                                        data-toggle="modal" data-target="#view{{ $country->id }}">
                                                    <i class="fa fa-eye"></i></button>
                                                <button class="btn btn-light btn-sm" data-toggle="modal"
                                                        data-target="#restore{{ $country->id }}">Restore
                                                </button>
                                            </div>

                                        </td>
                                    </tr>

                                    <div id="restore{{ $country->id }}" class="modal fade" tabindex="-1" role="dialog"
                                         aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content modal-filled">
                                                <div class="modal-body p-4">
                                                    <form method="POST"
                                                          action="{{ route('admin.countries.restore', ['country' => $country->id]) }}">
                                                        @method('PATCH')
                                                        @csrf
                                                        <div class="text-center">
                                                            <i class="dripicons-warning h1 text-warning"></i>
                                                            <h4 class="mt-2 ">Restore {{ $country->name }}
                                                                !</h4>
                                                            <p class="mt-3">
                                                                Are you sure you want to restore this country?
                                                                All projects under this country will be made
                                                                available.
                                                                but this action is reversible</p>
                                                            <button type="submit" class="btn btn-warning my-2">Proceed
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                @endif

                                <div class="modal fade" id="view{{ $country->id }}" tabindex="-1" role="dialog"
                                     style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myCenterModalLabel">Details</h4>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">×
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <h5>{{ $country->name }}</h5>
                                                <p>{{ $country->description ?: 'No Description' }}</p>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                            @empty
                                <tr>
                                    <td colspan="4" class="text-center">No Category Found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        @if(!$search['voided'])
                            <a href="{{ route('admin.countries.index', ['voided' => true]) }}"
                               class="small text-muted">Show deleted Countries</a>
                        @else
                            <a href="{{ route('admin.countries.index') }}"
                               class="small text-muted">Hide deleted Countries</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">New Country</h5>
                    <form method="POST" action="{{ route('admin.countries.store') }}">
                        @csrf

                        <div class="form-group">
                            <label>Name <em>*</em></label>
                            <input type="text" name="name"
                                   value="{{ old('name') }}"
                                   class="form-control @error('name') is-invalid @enderror">
                            @error('name')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        {{-- <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control @error('description') is-invalid @enderror"
                                      name="description" rows="5">{{ old('description') }}</textarea>
                            @error('description')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div> --}}

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer_script')
    <script></script>
@endsection
