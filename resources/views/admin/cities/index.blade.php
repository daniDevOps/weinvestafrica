@extends('admin.layouts.app')
@section('title', 'Cities')

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">Cities</li>
                    </ol>
                </div>
                <h4 class="page-title">Cities</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Cities</h5>
                    <div class="table-responsive">
                        <table class="table table-stripped">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Cities</td>
                                <td>Projects</td>
                                <td>Country</td>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($cities as $city)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $city->name }}</td>
                                    <td>{{ App\Project::where('city_id', $city->id)->where('status', 'approved')->count() }}</td>
                                    <td>{{ App\Country::find($city->country_id)->name }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4" class="text-center">No City Found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer_script')
    <script></script>
@endsection
