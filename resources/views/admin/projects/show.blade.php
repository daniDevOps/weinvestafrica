@extends('admin.layouts.app')
@section('title', $project->name)

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.projects.index') }}">Projects</a></li>
                        <li class="breadcrumb-item active">{{ $project->name }}</li>
                    </ol>
                </div>
                <h4 class="page-title">Project Detail</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row flex-column-reverse flex-md-row">
        <div class="col-md-7">
            <div class="card d-block">
                <div class="card-body">
                    <!-- project title-->
                    <h3 class="mt-0 font-20">
                        {{ $project->name }}
                    </h3>
                    @if($project->status == 'approved')
                        <span class="badge badge-success mb-3">Approved</span>
                    @elseif($project->status == 'pending')
                        <span class="badge badge-warning mb-3">Pending Review</span>
                    @endif

                    <h5>Project Overview:</h5>

                    <p class="mb-4 text-muted">
                        {!! $project->description !!}
                    </p>

                    <div class="mb-4">
                        <h5>Category</h5>
                        <div class="text-uppercase">
                            <a href="{{ route('admin.projects.index', ['category' => $project->category_id]) }}"
                               class="badge badge-soft-primary mr-1">{{ $project->category->name }}</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="mb-4">
                                <h5>Investment</h5>
                                <p>${{ number_format($project->investment)  }}</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="mb-4">
                                <h5>ROI</h5>
                                <p>{{ $project->roi }}x</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="mb-4">
                                <h5>Duration</h5>
                                <p>{{ $project->duration }}month{{ $project->duration != 1 ? 's' : '' }}</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="mb-4">
                                <h5>Country</h5>
                                <p>{{ $project->country }}</p>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="mb-4">
                                <h5>Address</h5>
                                <p>{{ $project->address }}</p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="mb-4">
                                <h5>Uploaded</h5>
                                <p>{{ date('j M Y,', strtotime($project->created_at)) }}
                                    <small class="text-muted">{{ date('g:i A', strtotime($project->created_at)) }}</small>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div>
                        <h5>Owner Contact:</h5>
                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title=""
                           data-original-title="Mat Helme" class="d-inline-block">
                            {{ $project->email ?: 'N/A' }}
                        </a>
                    </div>

                </div> <!-- end card-body-->

            </div>
        </div>
        <div class="col-md-5">

            <div class="card d-block">
                <img src="{{ $project->getImageURL() }}" class="img-fluid">
                <div class="card-body">
                    @if ($project->status === 'pending')
                        <div class="alert alert-warning" role="alert">
                            This project is currently pending approval, please review and either reject or approve this
                            project.
                        </div>

                        <div class="row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <button type="button" data-toggle="modal" data-target="#approveModal"
                                        class="btn btn-success btn-block waves-effect waves-light" onclick="window.location='{{ route('admin.appraisalMail', array($project->id, 'approved')) }}'">
                                    Approve<span class="btn-label-right"><i class="mdi mdi-check-all"></i></span>
                                </button>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" data-toggle="modal" data-target="#rejectModal"
                                        class="btn btn-danger btn-block waves-effect waves-light" onclick="window.location='{{ route('admin.appraisalMail', array($project->id, 'rejected')) }}'">
                                    Reject<span class="btn-label-right"><i
                                                class="mdi mdi-close-circle-outline"></i></span>
                                </button>
                            </div>
                        </div>

                        {{--Reject Modal--}}
                        <div id="rejectModal" class="modal fade" tabindex="-1" role="dialog"
                             aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content modal-filled bg-danger">
                                    <div class="modal-body p-4">
                                        <form method="POST"
                                              action="{{ route('admin.projects.update', ['project' => $project->id]) }}">
                                            @method('PUT')
                                            @csrf

                                            <input type="hidden" name="status" value="reject"/>

                                            <div class="text-center">
                                                <i class="dripicons-wrong h1 text-white"></i>
                                                <h4 class="mt-2 text-white">Reject {{ $project->name }}!</h4>
                                                <p class="mt-3 text-white">
                                                    Are you sure you want to reject this project?
                                                    All information about this project will be lost and
                                                    this action is reversible</p>
                                                <button type="submit" class="btn btn-light my-2">Proceed</button>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>

                        {{--Approve Modal--}}
                        <div id="approveModal" class="modal fade" tabindex="-1" role="dialog"
                             aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content modal-filled bg-success">
                                    <div class="modal-body p-4">
                                        <form method="POST"
                                              action="{{ route('admin.projects.update', ['project' => $project->id]) }}">
                                            @method('PUT')
                                            @csrf

                                            <input type="hidden" name="status" value="approve"/>

                                            <div class="text-center">
                                                <i class="dripicons-checkmark h1 text-white"></i>
                                                <h4 class="mt-2 text-white">Approve {{ $project->name }}!</h4>
                                                <p class="mt-3 text-white">
                                                    Are you sure you want to approve this project?
                                                    Approving this project will make it available on the home page for
                                                    investors.</p>
                                                <button type="submit" class="btn btn-light my-2">Proceed</button>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>
                    @else
                        <div class="alert alert-info" role="alert">
                            This project is currently active and is available for investors.
                        </div>
                    @endif


                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
    <script></script>
@endsection
