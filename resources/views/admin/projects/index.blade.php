@extends('admin.layouts.app')
@section('title', 'Projects')

@section('header-style')
    <style>
        .project-card .status {
            position: absolute;
            right: 10px;
            top: 10px;
        }
    </style>
@endsection

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">Projects</li>
                    </ol>
                </div>
                <h4 class="page-title"> {{ $search['status'] === 'pending' ? 'Pending Review' : 'Project List' }} </h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex">
                    <button type="button" class="btn btn-light btn-sm mr-1 text-dark"
                            data-toggle="collapse" data-target="#collapseFilter" title="Filter results">
                        <i class="mdi mdi-filter"></i>
                    </button>
                    <div class="dropdown">
                        <button class="btn btn-light btn-sm dropdown-toggle"
                                type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            {{ $search['limit'] }} <i class="mdi mdi-chevron-down"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="">
                            @foreach($limits as $limit)
                                <a class="dropdown-item"
                                   href="{{ route('admin.projects.index', ['q'=>$search['q'], 'category'=>$search['category'], 'status'=>$search['status'],  'sort'=>$search['sort'], 'limit'=>$limit]) }}">{{ $limit }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>

                <small>Page {{ $projects->currentPage() }} of {{ $projects->total() }} results</small>
            </div>

            <div class="collapse mt-3 {{ $search['q'] || $search['category'] || $search['sort'] ? 'show' : '' }}" id="collapseFilter" style="">
                <form method="GET" action="{{ route('admin.projects.index') }}">
                    <input type="hidden" name="limit" value="{{ $search['limit'] }}">
                    <input type="hidden" name="status" value="{{ $search['status'] }}">

                    <div class="row">
                        <div class="form-group col-md-4">
                            <input type="text" class="form-control"
                                   name="q" value="{{ $search['q'] }}"
                                   placeholder="Project name..."/>
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control" name="category">
                                <option value="">--Category--</option>
                                @foreach(\App\Category::orderBy('name', 'ASC')->get() as $category)
                                    <option value="{{ $category->id }}"
                                            @if($category->id == $search['category']) selected @endif>
                                        {{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control" name="sort">
                                <option value="">--Sort--</option>
                                @foreach($sort_list as $sort => $name)
                                    <option value="{{ $sort }}"
                                            @if($sort === $search['sort']) selected @endif>
                                        {!! $name !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2 ">
                            <button type="submit" class="btn btn-light btn-block"><i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>

    <div class="row">
        @forelse($projects as $project)
            <div class="col-md-4">
                <div class="card project-card">
                    @if($project->status == 'approved')
                        <span class="badge badge-success status">Approved</span>
                    @elseif($project->status == 'pending')
                        <span class="badge badge-warning status">Pending Review</span>
                    @endif

                    <a href="{{ route('admin.projects.show', ['project' => $project->id]) }}">
                        <img class="card-img-top img-fluid"
                             src="{{ $project->getImageURL() }}" alt="Card image cap">
                    </a>


                    <div class="card-body">
                        <a href="{{ route('admin.projects.show', ['project' => $project->id]) }}">
                            <h5 class="card-title overflow-ellipsis" title="{{ $project->name }}">{{ $project->name }}</h5>
                        </a>
                        <p class="card-text d-flex justify-content-between align-items-center">
                            <a href="{{ route('admin.projects.index', ['category' => $project->category_id]) }}"
                               class="badge badge-soft-primary overflow-ellipsis">{{ $project->category->name }}</a>
                            <small class="text-muted">Uploaded {{ $project->created_at->diffForHumans() }}</small>
                        </p>


                        <p class="card-text">{{ Str::limit($project->description, 100) }}</p>

                        <div class="card-text">
                            <table class="table table-sm table-striped table-borderless">
                                <tr>
                                    <th>Investment:</th>
                                    <td>${{ number_format($project->investment) }}</td>
                                </tr>
                                <tr>
                                    <th>ROI:</th>
                                    <td>{{ $project->roi }}x</td>
                                </tr>
                                <tr>
                                    <th>Duration:</th>
                                    <td>{{ $project->duration }}month{{ $project->duration != 1 ? 's' : '' }}</td>
                                </tr>
                            </table>

                            @if($search['status'] === 'pending')
                                <a href="{{ route('admin.projects.show', ['project' => $project->id]) }}" class="btn btn-block btn-sm btn-warning">Review</a>
                            @else
                                <a href="{{ route('admin.projects.show', ['project' => $project->id]) }}" class="btn btn-block btn-sm btn-primary">More Details</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @empty
        <div class="col-12 text-center">
            @if($search['status'] === 'pending')
                <h4>No Project pending review found</h4>
            @else
                <h4>No Projects found</h4>
                <p>Sorry, no project found. Please try using different search parameters</p>
            @endif
        </div>
        @endforelse
    </div>


    {{ $projects->appends(['q'=>$search['q'], 'category'=>$search['category'], 'status'=>$search['status'], 'sort'=>$search['sort'], 'limit'=>$search['limit']])->links() }}
@endsection

@section('footer_script')
    <script></script>
@endsection
