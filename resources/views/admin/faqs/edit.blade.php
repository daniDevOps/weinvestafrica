@extends('admin.layouts.app')
@section('title', $faq->question)

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.faqs.index') }}">Categories</a></li>
                        <li class="breadcrumb-item active">{{ $faq->question }}</li>
                    </ol>
                </div>
                <h4 class="page-title">Edit: {{ $faq->question }}</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Edit FAQ</h5>
            <form method="POST" action="{{ route('admin.faqs.update', ['faq' => $faq->id]) }}">
                @csrf
                @method('PATCH')

                <div class="form-group">
                    <label>Question <em>*</em></label>
                    <input type="text" name="question"
                           value="{{ old('question', $faq->question) }}"
                           class="form-control @error('question') is-invalid @enderror">
                    @error('question')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Answer</label>
                    <textarea class="form-control @error('answer') is-invalid @enderror"
                              name="answer" rows="5">{{ old('answer', $faq->answer) }}</textarea>
                    @error('answer')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>

@endsection

@section('footer_script')
    <script></script>
@endsection
