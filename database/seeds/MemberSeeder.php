<?php

use App\Member;
use Illuminate\Database\Seeder;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Member::count() > 0) {
            return;
        }

        $faker = \Faker\Factory::create();
        $storage_path = storage_path('app/public/uploads/users');
        \File::isDirectory($storage_path) or \File::makeDirectory($storage_path, 0777, true, true);

        for ($i = 0; $i < 10; $i++) {
            $mem = new Member();
            $mem->name = $faker->name;
            $mem->email = $faker->unique()->email;
            $mem->picture = $faker->file(public_path('samples/userImgs/' . mt_rand(1, 10)), $storage_path, false);
            $mem->position = $faker->title;
            $mem->description = $faker->text(300);
            $mem->save();
        }

    }
}