<?php

use Illuminate\Database\Seeder;
use App\Admin;
use Illuminate\Support\Facades\Hash;


class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Admin::count() > 0) {
            return;
        }

        $admin = new Admin();
        $admin->name = 'WeInvest Admin';
        $admin->email = 'admin@weinvest.com';
        $admin->phone = '6123456';
        $admin->password = Hash::make('password');
        $admin->super_admin = true;
        $admin->save();
    }
}
