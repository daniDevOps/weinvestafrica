<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Category::count() > 0) {
            return;
        }

        $categories = [
            'Agriculture', 'Technology', 'Education', 'Politics', 'Humanity', 'Environment', 'Medicine'
        ];
        $faker = \Faker\Factory::create();

        foreach ($categories as $category) {
            $cat = new Category();
            $cat->name = $category;
            $cat->description = $faker->text(300);
            $cat->save();
        }
    }
}