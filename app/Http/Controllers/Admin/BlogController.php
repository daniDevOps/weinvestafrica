<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    protected $search = [
        'q' => null,
        'sort' => null,
        'limit' => 24,
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $this->search;
        $posts = new Blog();

        if ($request->get('q')) {
            $search['q'] = $request->get('q');
            $posts = $posts->where('title', 'like', '%' . $search['q'] . '%')->orderBy('created_at', 'desc');
        }

        if ($request->get('limit')) {
            $search['limit'] = $request->get('limit');
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
            switch ($sort) {
                case 'name_asc':
                    $posts = $posts->orderBy('name', 'ASC');
                    break;

                case 'name_desc':
                    $posts = $posts->orderBy('name', 'DESC');
                    break;

                case 'newest':
                    $posts = $posts->orderBy('created_at', 'DESC');
                    break;

                case 'oldest':
                    $posts = $posts->orderBy('created_at', 'ASC');
                    break;
                default:
                    $posts = $posts->orderBy('created_at', 'desc');
            }
            $search['sort'] = $sort;
        }

        $posts = $posts->paginate($search['limit']);

        $sort_list = [
            'newest' => 'Newest',
            'oldest' => 'Oldest',
            'name_asc' => 'Name (A - Z)',
            'name_desc' => 'Name (Z - A)',
        ];
        $limits = [12, 24, 50, 100];

        return view('admin.blogs.index', compact('posts', 'search', 'sort_list', 'limits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required|unique:blogs,title',
            'image' => 'image|nullable|max:1999',
            'description' => 'required',
        ]);

        $post = new Blog();
        $post->title = $request->title;
        $post->post_image = $this->getImageInfo($request);
        $post->description = $request->description;
        $post->save();

        return redirect()->route('admin.blogs.index')
            ->with('success', 'Post created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Blog::findOrFail($id);
        return view('admin.blogs.show', compact('post'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Blog::findOrFail($id);
        return view('admin.blogs.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Blog::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'image' => 'image|nullable|max:1999',
            'description' => 'nullable',
        ]);

        // Check to see if another post already exist with the same name but different id
        $validator->after(function ($validator) use ($request, $post) {
            $existing_title = Blog::where('title', $request->title)->first();
            if ($existing_title && $existing_title->id != $post->id) {
                $validator->errors()->add('title', __('This title has already been taken'));
            }
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $post->title = $request->title;

        if ($request->has('image')) {
            $post->post_image = $this->getImageInfo($request);
        }

        $post->description = $request->description;
        $post->save();

        return redirect()->route('admin.blogs.index')
            ->with('success', 'Post updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Blog::findOrFail($id);

        // unlink(storage_path('app/public/uploads/posts/' . $post->post_image));

        $post->delete();

        return redirect()->route('admin.blogs.index')->with('success', 'Post deleted successfully');

    }

    private function getImageInfo(Request $request)
    {
        // handle the file upload

        $fileNameToStore = '';
        if ($request->hasFile('image')) {
            // get the file name with extension
            $fileNameWithExt = $request->file('image')->getClientOriginalName();
            // get the filename
            $fileName = pathInfo($fileNameWithExt, PATHINFO_FILENAME);
            // get the extension
            $extension = $request->file('image')->getClientOriginalExtension();
            // filename to store
            $fileNameToStore = Str::slug($fileName . '_' . time()) . '.' . $extension;
            // upload image

            $request->file('image')->storeAs('public/uploads/posts', $fileNameToStore);
        }

        return $fileNameToStore;
    }

    public function renderImage($filename)
    {
        $path = storage_path('app/public/uploads/posts/' . $filename);

        //if (!\File::exists($path)) abort(404);
        $file = \File::get($path);
        $type = \File::mimeType($path);
        $response = \Response::make($file, 200);
        $response->header('Content-Type', $type);
        return $response;
    }
}