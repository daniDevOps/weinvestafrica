<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    protected $search = [
        'q' => null,
        'status' => null,
        'sort' => null,
        'category' => null,
        'country' => null,
        'limit' => 24
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $this->search;
        $projects = new Project();

        if ($request->get('status')) {
            $search['status'] = $request->get('status');
            $projects = $projects->where('status', $search['status']);
        }

        if ($request->get('q')) {
            $search['q'] = $request->get('q');
            $projects = $projects->where('name', 'like', '%' . $search['q'] . '%');
        }

        if ($request->get('category')) {
            $search['category'] = $request->get('category');
            $projects = $projects->where('category_id', $search['category']);
        }

        if ($request->get('country')) {
            $search['country'] = $request->get('country');
            $projects = $projects->where('country_id', $search['country']);
        }

        if ($request->get('limit')) {
            $search['limit'] = $request->get('limit');
        }


        if ($request->get('sort')) {
            $sort = $request->get('sort');
            switch ($sort) {
                case 'name_asc':
                    $projects = $projects->orderBy('name', 'ASC');
                    break;

                case 'name_desc':
                    $projects = $projects->orderBy('name', 'DESC');
                    break;

                case 'investment_asc':
                    $projects = $projects->orderBy('investment', 'ASC');
                    break;

                case 'investment_desc':
                    $projects = $projects->orderBy('investment', 'DESC');
                    break;

                case 'newest':
                    $projects = $projects->orderBy('created_at', 'DESC');
                    break;

                case 'oldest':
                    $projects = $projects->orderBy('created_at', 'ASC');
                    break;
            }
            $search['sort'] = $sort;
        }

        $projects = $projects->paginate($search['limit']);

        $sort_list = [
            'newest'     => 'Newest',
            'oldest'     => 'Oldest',
            'name_asc'   => 'Name (A - Z)',
            'name_desc'  => 'Name (Z - A)',
            'investment_asc'  => 'Investment (Low &gt; High)',
            'investment_desc' => 'Investment (High &gt; Low)',
        ];
        $limits = [12, 24, 50, 100];

        return view('admin.projects.index', compact('projects', 'search', 'sort_list', 'limits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);
        return view('admin.projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::findOrFail($id);

        if ($project->status === 'pending') {
            if ($request->get('status') === 'approve') {
                $project->status = 'approved';
                $project->save();

                return redirect()->back()->with('success', 'Project approved successfully');
            } elseif ($request->get('status') === 'reject') {
                if ($project->project_image) {
                    try {
                        unlink(storage_path('app/public/uploads/projects/' . $project->project_image));
                    } catch (\Exception $e) {
                    }
                }

                // TODO send an email to the owner of the project

                $project->delete();
                return redirect()->route('admin.projects.index')->with('success', 'Project deleted successfully');
            }
        }

        session()->flash('error', 'Sorry, you can not update an already approved project');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}