<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TeamController extends Controller
{
    protected $search = [
        'q' => null,
        'position' => null,
        'sort' => null,
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $this->search;
        $members = new Member();

        if ($request->get('q')) {
            $search['q'] = $request->get('q');
            $members = $members->where('name', 'like', '%' . $search['q'] . '%');
        }

        if ($request->get('position')) {
            $search['position'] = $request->get('position');
            $members = $members->where('id', $search['position']);
        }

        if ($request->get('sort')) {
            $sort = $request->get('sort');
            switch ($sort) {
                case 'name_asc':
                    $members = $members->orderBy('name', 'ASC');
                    break;

                case 'name_desc':
                    $members = $members->orderBy('name', 'DESC');
                    break;

                case 'newest':
                    $members = $members->orderBy('created_at', 'DESC');
                    break;

                case 'oldest':
                    $members = $members->orderBy('created_at', 'ASC');
                    break;
            }
            $search['sort'] = $sort;
        }

        $members = $members->get();
        $sort_list = [
            'newest' => 'Newest',
            'oldest' => 'Oldest',
            'name_asc' => 'Name (A - Z)',
            'name_desc' => 'Name (Z - A)',
        ];

        return view('admin.team.index', compact('members', 'search', 'sort_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.team.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|unique:members,name',
            'email' => 'required|unique:members,email',
            'position' => 'required',
            'image' => 'image|nullable|max:1999',
            'description' => 'required',
        ]);

        $member = new Member();
        $member->name = $request->name;
        $member->email = $request->email;
        $member->position = $request->position;
        $member->picture = $this->getImageInfo($request);
        $member->description = $request->description;
        $member->save();

        return redirect()->route('admin.team.index')
            ->with('success', 'Member added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::findOrFail($id);
        return view('admin.team.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::findOrFail($id);
        return view('admin.team.edit', compact('member'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'position' => 'required',
            'image' => 'image|nullable|max:1999',
            'description' => 'nullable',
        ]);

        // Check to see if another member already exist with the same name but different id
        $validator->after(function ($validator) use ($request, $member) {
            $existing_title = Member::where('name', $request->title)->first();
            if ($existing_title && $existing_title->id != $member->id) {
                $validator->errors()->add('name', __('This name has already been taken'));
            }
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $member->name = $request->name;
        $member->email = $request->email;
        $member->position = $request->position;
        if ($request->has('image')) {
            $member->picture = $this->getImageInfo($request);
        }

        $member->description = $request->description;
        $member->save();

        return redirect()->route('admin.team.index')
            ->with('success', 'Team member updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::findOrFail($id);

        // unlink(storage_path('app/public/uploads/users/' . $member->member_image));

        $member->delete();

        return redirect()->route('admin.team.index')->with('success', 'Team member deleted successfully');

    }

    private function getImageInfo(Request $request)
    {
        // handle the file upload

        $fileNameToStore = '';
        if ($request->hasFile('image')) {
            // get the file name with extension
            $fileNameWithExt = $request->file('image')->getClientOriginalName();
            // get the filename
            $fileName = pathInfo($fileNameWithExt, PATHINFO_FILENAME);
            // get the extension
            $extension = $request->file('image')->getClientOriginalExtension();
            // filename to store
            $fileNameToStore = Str::slug($fileName . '_' . time()) . '.' . $extension;
            // upload image

            $request->file('image')->storeAs('public/uploads/users', $fileNameToStore);
        }

        return $fileNameToStore;
    }

    public function renderImage($filename)
    {
        $path = storage_path('app/public/uploads/users/' . $filename);

        //if (!\File::exists($path)) abort(404);
        $file = \File::get($path);
        $type = \File::mimeType($path);
        $response = \Response::make($file, 200);
        $response->header('Content-Type', $type);
        return $response;
    }
}